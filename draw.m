function im = draw(vec, draw)
    im = vec2mat(vec, 28);
    if (draw)
        figure(1);
        imshow(mat2gray(im) );
    end 