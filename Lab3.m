clear;
clc;
close all;
%% 20

load dataTransform;
[trainInd1,valInd1,testInd1] = dividerand(42000, .7, .2, .1);


Xtrain = X1(trainInd1, :);
Ttrain = T1(trainInd1, :);

Xval = [X1(valInd1, :)];
Tval = [T1(valInd1, :)];

Xtest = [X1(testInd1, :)];
Ttest = [T1(testInd1, :)];

X = [Xtrain; Xval];
T = [Ttrain; Tval];

%% Game with signal (������������� �� ��������)
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % [~, Xh, Xv] = Transform([Xtrain; Xval]);
% % [~, XhTest, XvTest] = Transform(Xtest);
% % XhTest = cell2mat(XhTest');
% % XvTest = cell2mat(XvTest)';
% % Xtest = [XhTest, XvTest];
% % Xh = cell2mat(Xh');
% % Xv = cell2mat(Xv)';
% % X = [Xh, Xv];
% % T = [Ttrain; Tval];
% % load Game
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% in = [];
% out = [];
% for i = 1 : size(X, 2)
%     in = [in; 0 255];
% end
% for i = 1 : 10
%     out = [out; 0 1];
% end
% 
%% �������� ��������
% 
% % ������ �� �������� ������������, � ������ ������
% % ��� ����� performance?
% net = feedforwardnet(100, 'traincgb');
% net.numLayers = 3;
% net.outputConnect = [0 0 1];
% net.layerConnect = [0 0 0; 1 0 0; 0 1 0];
% net.biasConnect = [1; 1; 1];
% net.layers{1}.transferFcn = 'purelin';
% net.layers{2}.transferFcn = 'tansig';
% net.layers{3}.transferFcn = 'purelin';
% net.layers{1}.name = 'Input';
% net.layers{2}.name = 'Hidden';
% net.layers{3}.name = 'Output';
% net.layers{2}.size = 80;
% net = configure(net, in, out);
% net.divideFcn = 'dividetrain'; % ��� ��� �����?
% % net.divideParam.trainInd = 1 : ntrain;
% % net.divideParam.valInd = ntrain + 1 : ntrain + nval;
% % net.divideParam.testInd = ntrain + nval + 1 : ntrain + nval + ntest;
% X = X';
% T = T';
% Xtest = Xtest';
% Ttest = Ttest';
% net = init(net);
% 
% net.trainParam.epochs = 1000;
% net.trainParam.max_fail = 10000;
% net.trainParam.goal = 1e-6;
% net.trainParam.min_grad = 1e-10;
% net = train(net, X, T);

load net.trainscg.mat;
check = 0;
[ans, ind] = max(net(Xtest'));
for i = 1 : size(Ttest, 1)
    check = check + (Ttest(i, ind(i)) == 1);
end

check



% load dataTest
% X = test.data;
% [answ, ind] = max(net(X'));