function [image, verticalDiagram, horizontalDiagram] = Transform(X1)
    image = {};
    for i = 1 : size(X1, 1)
        image{i} = vec2mat(X1(i, 1 : end), 28);
        image{i} = mat2gray(image{i});
        verticalDiagram{i} = sum(vec2mat(image{i}, 28), 1);
        horizontalDiagram{i} = sum(vec2mat(image{i}, 28), 2);
    end