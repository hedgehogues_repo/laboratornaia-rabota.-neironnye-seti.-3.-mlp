function [X1, T1, X2] = LoadCsv(isSave)
    train = importdata('train.csv');
    test = importdata('test.csv');
    X1 = train.data(:, 2 : end);
    X2 = test.data(:, 1 : end);
    T1 = train.data(:, 1);
    clear train;
    clear test;
    if (isSave)
        save data;
    end