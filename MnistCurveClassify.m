load dataTransform;
i = 9871;
[ansV, ansH, vX, hX] = Classify(X1(i, :), verticalDiagram, horizontalDiagram);
[~, mV] = min(cell2mat(ansV) );
[~, mH] = min(cell2mat(ansH) );
figure(2);
subplot(2, 2, 1);
plot(1 : 10, cell2mat(ansV), 'r', 1 : 10, cell2mat(ansH), 'b');
subplot(2, 2, 2);
plot(1 : 28, vX{1}, 'r', 1 : 28, verticalDiagram{5}, 'b');
subplot(2, 2, 3);
plot(1 : 28, hX{1}, 'r', 1 : 28, horizontalDiagram{5}, 'b');
draw(X1(i, :), 1);