function [image, imagesRes, verticalDiagram, horizontalDiagram, T1] = TransformTarget(isSave, T1, X1)
    P = zeros(42000, 10);
    image = {};
    imagesRes = {};
    for i = 1 : 10
        image{i} = find((T1 == i - 1) == 1);
        imagesRes{i} = sum(X1(image{i}, :), 1);
        imagesRes{i} = mat2gray(imagesRes{i});
        verticalDiagram{i} = sum(vec2mat(imagesRes{i}, 28), 1);
        horizontalDiagram{i} = sum(vec2mat(imagesRes{i}, 28), 2);
        P(:, i) = (T1 == i - 1);
    end
    T1 = P;
    clear P;
    clear i;
    if (isSave)
        save dataTransform;
    end