function [ansV, ansH, vX, hX] = Classify(X, v, h)
    [~, vX, hX] = Transform(X);
    for i = 1 : 10
        devV = abs(vX{1} - v{i});
        devH = abs(hX{1} - h{i});
        ansV{i} = (devV(1) + devV(end)) / 2 + sum(devV(2 : end - 1) );
        ansH{i} = (devH(1) + devH(end)) / 2 + sum(devH(2 : end - 1) );
    end